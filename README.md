# Atom feed 

Simple atom feed example

See the [Posts](posts.xml) and [Broadcasts](broadcasts.xml) files,
validate feed with [W3C Feed Valitation](https://validator.w3.org/feed)

Check if your web server uses `Content-Type "application/atom+xml"` and http status code `200`

More information at [Atom Syndication](https://www.w3.org/2005/Atom)
